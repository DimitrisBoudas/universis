import { RegistrarSharedModule } from './registrar-shared.module';
import 'jasmine';
describe('RegistrarSharedModule', () => {
  let registrarSharedModule: RegistrarSharedModule;

  beforeEach(() => {
    registrarSharedModule = new RegistrarSharedModule(null);
  });

  it('should create an instance', () => {
    expect(registrarSharedModule).toBeTruthy();
  });
});
